<?php  

require 'config.inc.php';

class OrcidService {

	private $token;
	var $client_id;
	var $client_secret;
	var $host;

	public function __construct()
	{
			$this->client_id =  constant("ORCID_CLIENT_ID");
			$this->client_secret = constant("ORCID_CLIENT_SECRET");
			$this->host = constant("ORCID_HOST");
			$this->token = $this->getToken();
	}

	public function getOrcidID($author_name)
	{
		$result = "";
	    $name_query = urlencode('"'.$author_name.'"');
	  
	    $url = $this->host."v3.0/search/?q=".$name_query."&start=0&rows=1";
	    
		// use key 'http' even if you send the request to https://...
		$options = array(
		    'http' => array(
		        'method'  => 'GET',
		        'header'  => "accept: application/json\r\n",
		    )
		);

		$context  = stream_context_create($options);
		$resp = file_get_contents($url, false, $context);
		if ($resp !== FALSE AND !is_null($resp)) {
			$orcid = json_decode($resp);
			
			if (isset($orcid->result[0]->{'orcid-identifier'})) {
				$orcid_identifier = $orcid->result[0]->{'orcid-identifier'};
				$result = $orcid_identifier->path;		
			} 
		}

	    return $result;

	}

	private function getToken()
	{
		$url = $this->host."oauth/token";
		$data = array(
					'client_id' => $this->client_id, 
					'client_secret' => $this->client_secret,
					'grant_type' => 'client_credentials',
					'scope' => '/read-public'
					);

		// use key 'http' even if you send the request to https://...
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data)
		    )
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		if ($result !== FALSE AND !is_null($result)) { 
			$orcid = json_decode($result, true);
			return $orcid['access_token'];
		}
	}
}